package com.example.farmassistant;

import java.util.Calendar;
import java.util.Date;

public class AppResources {

    static {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2000, 0, 1);

        MIN_BIRTH_DATE = calendar.getTime();
    }

    public static final String cowsCollectionName = "cows";
    public static final String cowsOwnerIdFieldName = "ownerId";
    public static final String cowsNameFieldName = "name";
    public static final String cowsNumberFieldName = "number";
    public static final String cowsBirthDateFieldName = "birthDate";
    public static final String cowsMotherIdFieldName = "motherId";
    public static final String cowsCommentFieldName = "comment";

    public static final String eventsCollectionName = "events";
    public static final String eventsOwnerIdFieldName = "ownerId";
    public static final String eventsDateFieldName = "date";
    public static final String eventsCowIdFieldName = "cowId";
    public static final String eventsTypeFieldName = "type";
    public static final String eventsCommentFieldName = "comment";

    public static final String DATE_FORMAT = "dd-MM-yyyy";

    public static final String COW_ID_EXTRA_NAME = "cow_id_extra_name";
    public static final String EVENT_ID_EXTRA_NAME = "event_id_extra_name";

    public static final String NOTIFICATIONS_CHANNEL_ID = "notifications_channel_id";

    public static final Date MIN_BIRTH_DATE;

    public static final int DAYS_FROM_BIRTH_TO_SEASON = 70;
    public static final int DAYS_FROM_SEASON_TO_SEASON = 21;
    public static final int DAYS_FROM_IMPREGNATION_TO_DRYING = 210;
    public static final int DAYS_FROM_IMPREGNATION_TO_BIRTH = 270;
    public static final int DAYS_TO_BIRTH = 14;
}
