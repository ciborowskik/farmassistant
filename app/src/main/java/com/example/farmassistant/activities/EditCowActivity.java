package com.example.farmassistant.activities;


import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.view.View;

import com.example.farmassistant.AppResources;
import com.example.farmassistant.R;
import com.example.farmassistant.dataSource.CowDataSource;
import com.example.farmassistant.databinding.ActivityEditCowBinding;
import com.example.farmassistant.viewModels.EditCowViewModel;

import java.util.Calendar;
import java.util.Date;

public class EditCowActivity extends BaseActivity {

    private ActivityEditCowBinding binding;

    private EditCowViewModel viewModel;
    private String cowId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_cow);

        cowId = getIntent().getStringExtra(AppResources.COW_ID_EXTRA_NAME);
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateViewModel();
    }

    public void onChooseCowClicked(View v) {
        this.showDropDown(R.string.choose_cow, viewModel.getCows(), cow -> viewModel.setMother(cow));
    }

    public void onSelectDateClicked(View v) {
        this.showDatePicker(viewModel.getBirthDate(), date -> viewModel.setBirthDate(date));
    }

    public void onSubmitClicked(View view){
        if (!isViewModelValid()) {
            return;
        }

        if (cowId == null) {
            CowDataSource.addCow(viewModel, newId -> {
                if (newId != null) {
                    notify(R.string.cow_created);
                    updateViewModel();
                } else {
                    notify(R.string.error_saving_data);
                }
            });
        }
        else {
            CowDataSource.editCow(viewModel, added -> {
                if (added) {
                    notify(R.string.cow_updated);
                    updateViewModel();
                } else {
                    notify(R.string.error_saving_data);
                }
            });
        }
    }

    private boolean isViewModelValid() {
        if (viewModel.getName().length() == 0) {
            notify(R.string.name_required);
            return false;
        }

        if (viewModel.getName().length() > 20) {
            notify(R.string.name_too_long);
            return false;
        }

        if (viewModel.getNumber().length() > 0 && viewModel.getNumber().length() != 12){
            notify(R.string.number_12_chars);
            return false;
        }

        if (viewModel.getBirthDate() == null){
            notify(R.string.birth_date_required);
            return false;
        }

        if (viewModel.getBirthDate().after(new Date())){
            notify(R.string.future_date_not_allowed);
            return false;
        }

        if (viewModel.getBirthDate().before(AppResources.MIN_BIRTH_DATE)){
            notify(R.string.date_too_low);
            return false;
        }

        if (viewModel.getComment().length() > 1000){
            notify(R.string.comment_too_long);
            return false;
        }

        return true;
    }

    private void updateViewModel() {
        CowDataSource.getCowsViewModels(cows -> {
            if (cows == null){
                notify(R.string.error_getting_data);
            }
            else {
                if (cowId == null) {
                    viewModel = new EditCowViewModel();
                    viewModel.setCows(cows);
                    setupToolbar(null);
                    binding.setViewModel(viewModel);
                    binding.executePendingBindings();
                }
                else {
                    CowDataSource.getCowDetailsViewModel(cowId, cow -> {
                        if (cow == null) {
                            notify(R.string.error_getting_data);
                        } else {
                            cows.removeIf(c -> c.getId().equals(cowId));
                            viewModel = new EditCowViewModel(cow);
                            viewModel.setCows(cows);
                            setupToolbar(cow.toString());
                            binding.setViewModel(viewModel);
                            binding.executePendingBindings();
                        }
                    });
                }
            }
        });
    }
}