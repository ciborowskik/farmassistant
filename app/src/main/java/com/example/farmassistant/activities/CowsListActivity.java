package com.example.farmassistant.activities;

import android.os.Bundle;

import com.example.farmassistant.R;
import com.example.farmassistant.dataSource.CowDataSource;
import com.example.farmassistant.fragments.CowsListFragment;

import java.util.Collections;

public class CowsListActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cows_list);
        setupToolbar(null);
    }

    @Override
    protected void onResume() {
        super.onResume();

        CowDataSource.getCowsViewModels(cows -> {
            if (cows == null){
                notify(R.string.error_getting_data);
            }
            else {
                inflateFragment(R.id.fragment, new CowsListFragment(cows, true), null);
            }
        });
    }
}