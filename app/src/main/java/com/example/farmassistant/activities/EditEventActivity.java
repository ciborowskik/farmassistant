package com.example.farmassistant.activities;


import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.example.farmassistant.AppResources;
import com.example.farmassistant.R;
import com.example.farmassistant.dataSource.CowDataSource;
import com.example.farmassistant.dataSource.EventDataSource;
import com.example.farmassistant.databinding.ActivityEditEventBinding;
import com.example.farmassistant.viewModels.EditEventViewModel;
import com.example.farmassistant.viewModels.EventType;

import java.util.Arrays;
import java.util.Date;

public class EditEventActivity extends BaseActivity {

    private ActivityEditEventBinding binding;

    private EditEventViewModel viewModel;
    private String cowId;
    private String eventId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_event);
        setupToolbar(null);

        cowId = getIntent().getStringExtra(AppResources.COW_ID_EXTRA_NAME);
        eventId = getIntent().getStringExtra(AppResources.EVENT_ID_EXTRA_NAME);
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateViewModel();
    }


    public void onChooseCowClicked(View v) {
        if (cowId != null || eventId != null){
            return;
        }

        this.showDropDown(R.string.choose_cow, viewModel.getCows(), cow -> viewModel.setCow(cow));
    }

    public void onChooseTypeClicked(View v) {
        this.showDropDown(R.string.choose_event_type, viewModel.getEventTypes(), eventType -> viewModel.setType(eventType));
    }

    public void onSelectDateClicked(View v) {
        this.showDatePicker(viewModel.getDate(), date -> viewModel.setDate(date));
    }

    public void onSubmitClicked(View view){
        if (!isViewModelValid()) {
            return;
        }

        if (eventId != null) {
            EventDataSource.editEvent(viewModel, updated -> {
                if (updated) {
                    notify(R.string.event_updated);
                    updateViewModel();
                } else {
                    notify(R.string.error_saving_data);
                }
            });
        }
        else {
            EventDataSource.addEvent(viewModel, newId -> {
                if (newId != null) {
                    notify(R.string.event_created);
                    updateViewModel();
                } else {
                    notify(R.string.error_saving_data);
                }
            });
        }
    }

    private boolean isViewModelValid() {
        if (viewModel.getCow() == null) {
            notify(R.string.cow_required);
            return false;
        }

        if (viewModel.getType() == null) {
            notify(R.string.type_required);
            return false;
        }

        if (viewModel.getDate() == null){
            notify(R.string.date_required);
            return false;
        }

        if (viewModel.getDate().after(new Date())){
            notify(R.string.future_date_not_allowed);
            return false;
        }

        if (viewModel.getCow() != null && viewModel.getDate().before(viewModel.getCow().getBirthDate())){
            notify(R.string.date_too_low);
            return false;
        }

        if (viewModel.getCow() == null && viewModel.getDate().before(AppResources.MIN_BIRTH_DATE)) {
            notify(R.string.date_too_low);
            return false;
        }

        if (viewModel.getComment().length() > 1000){
            notify(R.string.comment_too_long);
            return false;
        }

        return true;
    }

    private void updateViewModel() {
        if (cowId != null) {
            CowDataSource.getCowDetailsViewModel(cowId, cow -> {
                if (cow == null) {
                    notify(R.string.error_getting_data);
                } else {
                    viewModel = new EditEventViewModel();
                    viewModel.setCow(cow);
                    viewModel.setEventTypes(Arrays.asList(EventType.values()));
                    binding.setViewModel(viewModel);
                    binding.executePendingBindings();
                }
            });
        }
        else if (eventId != null) {
            EventDataSource.getEventViewModel(eventId, event -> {
                if (event == null) {
                    notify(R.string.error_getting_data);
                }
                else {
                    viewModel = new EditEventViewModel(event);
                    viewModel.setEventTypes(Arrays.asList(EventType.values()));
                    binding.setViewModel(viewModel);
                    binding.executePendingBindings();
                }
            });
        }
        else {
            CowDataSource.getCowsViewModels(cows -> {
                if (cows == null) {
                    notify(R.string.error_getting_data);
                }
                else {
                    viewModel = new EditEventViewModel();
                    viewModel.setCows(cows);
                    viewModel.setEventTypes(Arrays.asList(EventType.values()));
                    binding.setViewModel(viewModel);
                    binding.executePendingBindings();
                }
            });
        }
    }
}