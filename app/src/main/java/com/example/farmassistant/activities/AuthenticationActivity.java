package com.example.farmassistant.activities;


import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import com.example.farmassistant.R;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;
import java.util.List;

public class AuthenticationActivity extends AppCompatActivity {

    private FirebaseUser user;
    private ActivityResultLauncher<Intent> activityResultLauncher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() ==  Activity.RESULT_OK) {
                    user = FirebaseAuth.getInstance().getCurrentUser();
                    runHomeActivity();
                }
                else {
                    Intent data = result.getData();
                    if(IdpResponse.fromResultIntent(data) != null)
                        showAuthenticationFailedAlert(IdpResponse.fromResultIntent(data).getError().getMessage());
                    else
                        finish();
                }
            });

        user = FirebaseAuth.getInstance().getCurrentUser();

        if(user == null)
            runAuthUi();
        else
            runHomeActivity();
    }


    private void runAuthUi(){
        if(!isNetworkAvailable()) {
            showAuthenticationFailedAlert(getString(R.string.no_internet));
            return;
        }

        // Choose authentication providers
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build(),
                new AuthUI.IdpConfig.GoogleBuilder().build());

        // Create intent and run activity
        Intent intent = AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setAvailableProviders(providers)
                .setLogo(R.drawable.ic_cow)
                .build();

        activityResultLauncher.launch(intent);
    }

    private void runHomeActivity(){
        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void showAuthenticationFailedAlert(String message){
        new AlertDialog.Builder(AuthenticationActivity.this)
            .setMessage(message)
            .setPositiveButton(R.string.try_again, (dialog, id) -> runAuthUi())
            .setNegativeButton(R.string.close_app, (dialog, id) -> finish())
            .create()
            .show();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
