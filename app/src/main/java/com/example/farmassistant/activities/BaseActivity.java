package com.example.farmassistant.activities;


import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.example.farmassistant.R;
import com.example.farmassistant.toDoActions.ToDoNotifications;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

public class BaseActivity extends AppCompatActivity {

    protected FirebaseUser user;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user = FirebaseAuth.getInstance().getCurrentUser();
    }

    private void signOut(){
        FirebaseAuth.getInstance().signOut();

        notify(R.string.signed_out);

        Intent intent = new Intent(this, AuthenticationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_sign_out) {
            signOut();
            return true;
        }

        if (id == R.id.action_show_notifications) {
            ToDoNotifications.showNotifications(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected void setupToolbar(String title) {
        Toolbar toolbar = findViewById(R.id.toolbar);

        if (title != null){
            toolbar.setTitle(title);
        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    protected void showDatePicker(Date date, Consumer<Date> dateConsumer) {
        Calendar calendar = Calendar.getInstance();

        if (date != null) {
            calendar.setTime(date);
        }

        int todayYear = calendar.get(Calendar.YEAR);
        int todayMonth = calendar.get(Calendar.MONTH);
        int todayDay = calendar.get(Calendar.DAY_OF_MONTH);

        new DatePickerDialog(this, (view, year, month, day) -> {
            Calendar selectedDate = Calendar.getInstance();
            selectedDate.set(year, month, day);

            dateConsumer.accept(selectedDate.getTime());
        }, todayYear, todayMonth, todayDay).show();
    }

    protected <T> void showDropDown(int titleResId, List<T> items, Consumer<T> itemConsumer){
        ArrayAdapter<T> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);

        new AlertDialog.Builder(this)
                .setTitle(getString(titleResId))
                .setSingleChoiceItems(adapter, -1, (dialog, which) -> {
                    T item = (T)((AlertDialog)dialog).getListView().getAdapter().getItem(which);

                    itemConsumer.accept(item);

                    dialog.dismiss();
                }).create().show();
    }

    protected void inflateFragment(int viewId, Fragment fragment, Bundle arguments)
    {
        fragment.setArguments(arguments);

        FragmentTransaction mFragmentTransaction = getSupportFragmentManager().beginTransaction();
        mFragmentTransaction.replace(viewId, fragment);
        mFragmentTransaction.commit();
    }

    protected void notify(int stringResource) {
        Toast.makeText(getApplicationContext(), stringResource, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
