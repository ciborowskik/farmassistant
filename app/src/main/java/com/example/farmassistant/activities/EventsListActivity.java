package com.example.farmassistant.activities;


import android.os.Bundle;

import com.example.farmassistant.R;
import com.example.farmassistant.dataSource.EventDataSource;
import com.example.farmassistant.fragments.EventsListFragment;

import java.util.Collections;

public class EventsListActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events_list);
        setupToolbar(null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventDataSource.getEventsViewModels(events -> {
            if (events == null){
                notify(R.string.error_getting_data);
            }
            else {
                inflateFragment(R.id.fragment, new EventsListFragment(events, true), null);
            }
        });
    }
}
