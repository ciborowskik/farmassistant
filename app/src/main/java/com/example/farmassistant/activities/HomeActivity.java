package com.example.farmassistant.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.farmassistant.R;

public class HomeActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setupToolbar(null);
    }

    public void onCowsListClicked(View view) {
        Intent intent = new Intent(this, CowsListActivity.class);
        startActivity(intent);
    }

    public void onEventsListClicked(View view) {
        Intent intent = new Intent(this, EventsListActivity.class);
        startActivity(intent);
    }

    public void onAddCowClicked(View view) {
        Intent intent = new Intent(this, EditCowActivity.class);
        startActivity(intent);
    }

    public void onAddEventClicked(View view) {
        Intent intent = new Intent(this, EditEventActivity.class);
        startActivity(intent);
    }

    public void onToDoActionsClicked(View view) {
        Intent intent = new Intent(this, ToDoListActivity.class);
        startActivity(intent);
    }
}