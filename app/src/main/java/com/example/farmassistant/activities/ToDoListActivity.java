package com.example.farmassistant.activities;


import android.os.Bundle;

import com.example.farmassistant.R;
import com.example.farmassistant.fragments.ToDoListFragment;
import com.example.farmassistant.toDoActions.ToDoActionsCreator;

public class ToDoListActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do_list);
        setupToolbar(null);
    }

    @Override
    protected void onResume() {
        super.onResume();

        ToDoActionsCreator.prepareToDoActions(this, actions -> {
            if (actions == null){
                notify(R.string.error_getting_data);
            }
            else {
                inflateFragment(R.id.fragment, new ToDoListFragment(actions), null);
            }
        });
    }
}
