package com.example.farmassistant.activities;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AlertDialog;

import com.example.farmassistant.AppResources;
import com.example.farmassistant.R;
import com.example.farmassistant.dataSource.CowDataSource;
import com.example.farmassistant.fragments.CowDetailsFragment;
import com.example.farmassistant.viewModels.CowDetailsViewModel;

public class CowDetailsActivity extends BaseActivity {

    private CowDetailsViewModel cow;
    private String cowId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cow_details);

        cowId = getIntent().getStringExtra(AppResources.COW_ID_EXTRA_NAME);
    }

    @Override
    protected void onResume() {
        super.onResume();

        CowDataSource.getCowDetailsViewModel(cowId, cow -> {
            if (cow == null){
                notify(R.string.error_getting_data);
            }
            else {
                this.cow = cow;
                setupToolbar(cow.toString());
                inflateFragment(R.id.fragment_pager, new CowDetailsFragment(cow), null);
            }
        });
    }

    public void onEditClicked(View view) {
        Intent intent = new Intent(this, EditCowActivity.class);
        intent.putExtra(AppResources.COW_ID_EXTRA_NAME, cowId);
        startActivity(intent);
    }

    public void onDeleteClicked(View view) {
        new AlertDialog.Builder(this)
                .setTitle(R.string.cow_deleting)
                .setMessage(R.string.ensure_cow_deleting)
                .setNegativeButton(R.string.no, (dialog, id) -> dialog.dismiss())
                .setPositiveButton(R.string.yes, (dialog, id) -> {
                    CowDataSource.deleteCowWithEvents(cowId, deleted -> {
                        if (deleted){
                            notify(R.string.cow_deleted);
                            dialog.dismiss();
                            finish();
                        }
                        else {
                            notify(R.string.error_deleting_data);
                        }
                    });
                })
                .create()
                .show();
    }

    public void onAddEventClicked(View view) {
        Intent intent = new Intent(this, EditEventActivity.class);
        intent.putExtra(AppResources.COW_ID_EXTRA_NAME, cowId);
        startActivity(intent);
    }

    public void onMotherClicked(View view) {
        Intent intent = new Intent(this, CowDetailsActivity.class);
        intent.putExtra(AppResources.COW_ID_EXTRA_NAME, cow.getMotherId());
        startActivity(intent);
    }
}
