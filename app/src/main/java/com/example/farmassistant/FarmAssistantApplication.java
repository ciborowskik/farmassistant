package com.example.farmassistant;


import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;

public class FarmAssistantApplication extends Application {

    private static Context context;

    public void onCreate() {
        super.onCreate();
        FarmAssistantApplication.context = getApplicationContext();
        createNotificationChannel();
    }

    public static Context getAppContext() {
        return FarmAssistantApplication.context;
    }

    private void createNotificationChannel() {
        CharSequence name = getString(R.string.notifications_channel_name);
        String description = getString(R.string.notifications_channel_description);
        int importance = NotificationManager.IMPORTANCE_DEFAULT;

        NotificationChannel channel = new NotificationChannel(AppResources.NOTIFICATIONS_CHANNEL_ID, name, importance);
        channel.setDescription(description);

        NotificationManager notificationManager = getSystemService(NotificationManager.class);
        notificationManager.createNotificationChannel(channel);
    }
}