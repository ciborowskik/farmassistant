package com.example.farmassistant.toDoActions;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.example.farmassistant.AppResources;
import com.example.farmassistant.R;
import com.example.farmassistant.activities.CowDetailsActivity;
import com.example.farmassistant.viewModels.CowViewModel;
import com.example.farmassistant.viewModels.ToDoActionViewModel;

import java.util.concurrent.atomic.AtomicInteger;

public class ToDoNotifications {
    private final static AtomicInteger atomicId = new AtomicInteger(0);

    public static void showNotifications(Context context) {
        ToDoActionsCreator.prepareToDoActions(context, actions -> {
            for (ToDoActionViewModel action : actions) {
                ToDoNotifications.showNotification(action.getCow(), action.getMessage(), context);
            }
        });
    }

    private static void showNotification(CowViewModel cow, String text, Context context) {
        Intent intent = new Intent(context, CowDetailsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra(AppResources.COW_ID_EXTRA_NAME, cow.getId());
        intent.setAction(Long.toString(System.currentTimeMillis()));

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        Bitmap icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.cow);

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context, AppResources.NOTIFICATIONS_CHANNEL_ID)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(text))
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setContentIntent(pendingIntent)
                        .setSmallIcon(R.drawable.ic_cow)
                        .setLargeIcon(icon)
                        .setContentTitle(cow.toString());

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

        notificationManager.notify(atomicId.incrementAndGet(), builder.build());
    }
}
