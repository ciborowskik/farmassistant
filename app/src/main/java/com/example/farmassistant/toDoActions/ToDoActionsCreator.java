package com.example.farmassistant.toDoActions;

import android.content.Context;

import com.example.farmassistant.AppResources;
import com.example.farmassistant.R;
import com.example.farmassistant.dataSource.CowDataSource;
import com.example.farmassistant.dataSource.EventDataSource;
import com.example.farmassistant.viewModels.CowViewModel;
import com.example.farmassistant.viewModels.EventType;
import com.example.farmassistant.viewModels.EventViewModel;
import com.example.farmassistant.viewModels.ToDoActionViewModel;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class ToDoActionsCreator {
    public static void prepareToDoActions(Context context, Consumer<List<ToDoActionViewModel>> onFinished) {
        CowDataSource.getCowsViewModels(cows -> {
            if (cows == null) {
                onFinished.accept(null);
            }
            else {
                EventDataSource.getEventsViewModels(events -> {
                    if (events == null) {
                        onFinished.accept(null);
                    }
                    else {
                        onFinished.accept(prepareToDoActions(cows, events, context));
                    }
                });
            }
        });
    }

    private static List<ToDoActionViewModel> prepareToDoActions(List<CowViewModel> cows, List<EventViewModel> events, Context context) {
        List<ToDoActionViewModel> actions = new ArrayList<>();

        for (CowViewModel cow : cows) {
            List<EventViewModel> cowEvents = events.stream().filter(e -> e.getCowId().equals(cow.getId())).collect(Collectors.toList());
            ToDoActionViewModel action = prepareToDoAction(cow, cowEvents, context);

            if (action != null) {
                actions.add(action);
            }
        }

        return actions;
    }

    private static ToDoActionViewModel prepareToDoAction(CowViewModel cow, List<EventViewModel> events, Context context) {
        List<EventViewModel> eventsWithoutVets =
                events.stream().filter(e -> e.getType() != EventType.VET).sorted().collect(Collectors.toList());

        if (eventsWithoutVets.isEmpty()) {
            return null;
        }

        EventViewModel last = eventsWithoutVets.get(0);

        if (last.getType() == EventType.BIRTH) {
            long days = ChronoUnit.DAYS.between(last.getDate().toInstant(), new Date().toInstant());

            if (days >= AppResources.DAYS_FROM_BIRTH_TO_SEASON) {
                return new ToDoActionViewModel(cow, String.format(context.getString(R.string.season_after_birth_alert), days));
            }
        }

        if (last.getType() == EventType.SEASON) {
            long days = ChronoUnit.DAYS.between(last.getDate().toInstant(), new Date().toInstant());

            if (days == AppResources.DAYS_FROM_SEASON_TO_SEASON) {
                return new ToDoActionViewModel(cow, String.format(context.getString(R.string.season_after_season_alert), days));
            }
        }

        if (last.getType() == EventType.IMPREGNATION) {
            long days = ChronoUnit.DAYS.between(last.getDate().toInstant(), new Date().toInstant());

            if (days == AppResources.DAYS_FROM_SEASON_TO_SEASON) {
                return new ToDoActionViewModel(cow, String.format(context.getString(R.string.season_after_impregnation_alert), days));
            }

            if (days >= AppResources.DAYS_FROM_IMPREGNATION_TO_DRYING) {
                long daysToBirth = AppResources.DAYS_FROM_IMPREGNATION_TO_BIRTH - days;

                return new ToDoActionViewModel(cow, String.format(context.getString(R.string.drying_alert), daysToBirth));
            }
        }

        if (last.getType() == EventType.DRYING) {
            Date lastImpregnationDate = findLastOfType(eventsWithoutVets, EventType.IMPREGNATION);

            if (lastImpregnationDate == null) {
                return null;
            }

            long days = ChronoUnit.DAYS.between(lastImpregnationDate.toInstant(), new Date().toInstant());
            long daysToBirth = AppResources.DAYS_FROM_IMPREGNATION_TO_BIRTH - days;

            if (daysToBirth <= AppResources.DAYS_TO_BIRTH) {
                return new ToDoActionViewModel(cow, String.format(context.getString(R.string.birth_alert), daysToBirth));
            }
        }

        return null;
    }

    private static Date findLastOfType(List<EventViewModel> events, EventType type) {
        List<EventViewModel> eventsWithType =
                events.stream().filter(e -> e.getType() == type).collect(Collectors.toList());

        if (eventsWithType.isEmpty()) {
            return null;
        }
        else {
            return eventsWithType.get(0).getDate();
        }
    }
}
