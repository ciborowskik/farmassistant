package com.example.farmassistant.adapters;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.farmassistant.fragments.CowDetailsFragment;
import com.example.farmassistant.fragments.CowInfoFragment;
import com.example.farmassistant.fragments.CowsListFragment;
import com.example.farmassistant.fragments.EventsListFragment;
import com.example.farmassistant.viewModels.CowDetailsViewModel;

public class CowDetailsAdapter extends FragmentStateAdapter {

    private final CowDetailsViewModel cow;

    public CowDetailsAdapter(Fragment fragment, CowDetailsViewModel cow) {
        super(fragment);
        this.cow = cow;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        if (position == CowDetailsFragment.COW_INFO_TAB_POSITION){
            return new CowInfoFragment(cow);
        }

        if (position == CowDetailsFragment.COW_EVENTS_TAB_POSITION){
            return new EventsListFragment(cow.getEvents(), false);
        }

        if (position == CowDetailsFragment.COW_CHILDREN_TAB_POSITION){
            return new CowsListFragment(cow.getChildren(), false);
        }

        return null;
    }

    @Override
    public int getItemCount() {
        return 3;
    }
}