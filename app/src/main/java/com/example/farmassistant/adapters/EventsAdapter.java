package com.example.farmassistant.adapters;


import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.farmassistant.OnItemClickedListener;
import com.example.farmassistant.R;
import com.example.farmassistant.databinding.EventItemBinding;
import com.example.farmassistant.viewModels.EventViewModel;

import java.util.List;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.ViewHolder> {

    private final List<EventViewModel> dataSet;
    private final OnItemClickedListener listener;
    private final Boolean showCowsNames;

    public EventsAdapter(List<EventViewModel> dataSet, Boolean showCowsNames, OnItemClickedListener listener) {
        this.dataSet = dataSet;
        this.showCowsNames = showCowsNames;
        this.listener = listener;
    }

    @NonNull
    @Override
    public EventsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        EventItemBinding binding = EventItemBinding.inflate(layoutInflater, parent, false);

        return new EventsAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(EventsAdapter.ViewHolder holder, int position) {
        holder.bind(dataSet.get(position), showCowsNames);

        final String id = dataSet.get(position).getId();
        holder.menuOptions.setTag(id);

        holder.menuOptions.setOnClickListener(view -> listener.onItemClick(view, (String)view.getTag()));
    }

    @Override
    public int getItemCount() {
        return dataSet == null ? 0 : dataSet.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final EventItemBinding binding;
        public TextView menuOptions;

        ViewHolder(EventItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.menuOptions = binding.getRoot().findViewById(R.id.eventMenu);
        }

        void bind(EventViewModel event, Boolean showCowsNames) {
            binding.setEvent(event);
            binding.setShowCowsNames(showCowsNames);
            binding.executePendingBindings();
        }
    }
}
