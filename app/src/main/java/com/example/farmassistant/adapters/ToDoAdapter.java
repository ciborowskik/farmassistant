package com.example.farmassistant.adapters;


import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.farmassistant.OnItemClickedListener;
import com.example.farmassistant.databinding.ToDoItemBinding;
import com.example.farmassistant.viewModels.ToDoActionViewModel;

import java.util.List;

public class ToDoAdapter extends RecyclerView.Adapter<ToDoAdapter.ViewHolder> {

    private final List<ToDoActionViewModel> dataSet;
    private final OnItemClickedListener listener;

    public ToDoAdapter(List<ToDoActionViewModel> dataSet, OnItemClickedListener listener) {
        this.dataSet = dataSet;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ToDoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        ToDoItemBinding binding = ToDoItemBinding.inflate(layoutInflater, parent, false);

        return new ToDoAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ToDoAdapter.ViewHolder holder, int position) {
        holder.bind(dataSet.get(position));

        final String cowId = dataSet.get(position).getCow().getId();
        holder.itemView.setTag(cowId);

        holder.itemView.setOnClickListener(view -> listener.onItemClick(view, (String)view.getTag()));
    }

    @Override
    public int getItemCount() {
        return dataSet == null ? 0 : dataSet.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final ToDoItemBinding binding;

        ViewHolder(ToDoItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(ToDoActionViewModel action) {
            binding.setAction(action);
            binding.executePendingBindings();
        }
    }
}
