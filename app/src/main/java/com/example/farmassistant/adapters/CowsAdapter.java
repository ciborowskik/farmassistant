package com.example.farmassistant.adapters;


import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.farmassistant.OnItemClickedListener;
import com.example.farmassistant.databinding.CowItemBinding;
import com.example.farmassistant.viewModels.CowViewModel;

import java.util.List;

public class CowsAdapter extends RecyclerView.Adapter<CowsAdapter.ViewHolder> {

    private final List<CowViewModel> cows;
    private final OnItemClickedListener listener;
    private final Boolean showImage;

    public CowsAdapter(List<CowViewModel> cows, Boolean showImage, OnItemClickedListener listener) {
        this.listener = listener;
        this.cows = cows;
        this.showImage = showImage;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        CowItemBinding binding = CowItemBinding.inflate(layoutInflater, parent, false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(cows.get(position), showImage);

        final String id = cows.get(position).getId();
        holder.itemView.setTag(id);

        holder.itemView.setOnClickListener(view -> listener.onItemClick(view, (String)view.getTag()));
    }

    @Override
    public int getItemCount() {
        return cows == null ? 0 : cows.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final CowItemBinding binding;

        ViewHolder(CowItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(CowViewModel cow, Boolean showImage) {
            binding.setCow(cow);
            binding.setShowImage(showImage);
            binding.executePendingBindings();
        }
    }
}
