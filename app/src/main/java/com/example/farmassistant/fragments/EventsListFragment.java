package com.example.farmassistant.fragments;


import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.farmassistant.AppResources;
import com.example.farmassistant.OnItemClickedListener;
import com.example.farmassistant.R;
import com.example.farmassistant.activities.EditEventActivity;
import com.example.farmassistant.adapters.EventsAdapter;
import com.example.farmassistant.dataSource.EventDataSource;
import com.example.farmassistant.viewModels.EventViewModel;

import java.util.List;

public class EventsListFragment extends Fragment implements OnItemClickedListener {

    private EventDataSource EventDataSource;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter<EventsAdapter.ViewHolder> adapter;


    private List<EventViewModel> events;
    private Boolean showCowsNames;

    public EventsListFragment(List<EventViewModel> events, boolean showCowsNames) {
        this.events = events;
        this.showCowsNames = showCowsNames;

        EventDataSource = new EventDataSource();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        return inflater.inflate(R.layout.recycler_view_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = requireView().findViewById(R.id.recycler_view);
        adapter = new EventsAdapter(events, showCowsNames, this);

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
    }

    public void onEditClicked(String eventId) {
        Intent intent = new Intent(getActivity(), EditEventActivity.class);
        intent.putExtra(AppResources.EVENT_ID_EXTRA_NAME, eventId);
        requireActivity().startActivity(intent);
    }

    public void onDeleteClicked(final String eventId) {
        new AlertDialog.Builder(getActivity())
                .setTitle(R.string.event_deleting)
                .setMessage(R.string.ensure_event_deleting)
                .setNegativeButton(R.string.no, (dialog, id) -> dialog.dismiss())
                .setPositiveButton(R.string.yes, (dialog, id) -> {
                    EventDataSource.deleteEvent(eventId, deleted -> {
                        if (deleted){
                            this.removeFromRecycleView(eventId);
                            Toast.makeText(getActivity(), R.string.event_deleted, Toast.LENGTH_LONG).show();
                        }
                        else {
                            Toast.makeText(getActivity(), R.string.error_deleting_data, Toast.LENGTH_LONG).show();
                        }
                        dialog.dismiss();
                    });
                })
                .create()
                .show();
    }

    @Override
    public void onItemClick(View view, String eventId) {
        PopupMenu popup = new PopupMenu(requireActivity(), view);

        popup.inflate(R.menu.menu_item);

        popup.setOnMenuItemClickListener(item -> {
            int itemId = item.getItemId();

            if (itemId == R.id.menu1) {
                onEditClicked(eventId);
                return true;
            } else if (itemId == R.id.menu2) {
                onDeleteClicked(eventId);
                return true;
            }

            return false;
        });

        popup.show();
    }

    private void removeFromRecycleView(String eventId) {
        int position = 0;
        while (!eventId.equals(events.get(position).getId())) {
            position++;
        }

        events.remove(position);
        recyclerView.removeViewAt(position);
        adapter.notifyItemRemoved(position);
        adapter.notifyItemRangeChanged(position, events.size());
    }
}

