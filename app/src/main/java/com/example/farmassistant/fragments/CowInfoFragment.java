package com.example.farmassistant.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.example.farmassistant.R;
import com.example.farmassistant.databinding.CowInfoFragmentBinding;
import com.example.farmassistant.viewModels.CowDetailsViewModel;

public class CowInfoFragment extends Fragment {

    private CowDetailsViewModel cow;

    public CowInfoFragment(CowDetailsViewModel cow) {
        this.cow = cow;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        CowInfoFragmentBinding binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.cow_info_fragment, container, false);

        binding.setCow(cow);
        binding.executePendingBindings();

        return binding.getRoot();
    }
}
