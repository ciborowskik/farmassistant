package com.example.farmassistant.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.example.farmassistant.FarmAssistantApplication;
import com.example.farmassistant.R;
import com.example.farmassistant.adapters.CowDetailsAdapter;
import com.example.farmassistant.viewModels.CowDetailsViewModel;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.util.HashMap;
import java.util.Map;

public class CowDetailsFragment extends Fragment {

    public static int COW_INFO_TAB_POSITION = 0;
    public static int COW_EVENTS_TAB_POSITION = 1;
    public static int COW_CHILDREN_TAB_POSITION = 2;

    public static final Map<Integer, String> tabs = new HashMap<>();

    static {
        tabs.put(COW_INFO_TAB_POSITION, FarmAssistantApplication.getAppContext().getString(R.string.info));
        tabs.put(COW_EVENTS_TAB_POSITION, FarmAssistantApplication.getAppContext().getString(R.string.events));
        tabs.put(COW_CHILDREN_TAB_POSITION, FarmAssistantApplication.getAppContext().getString(R.string.children));
    }

    private final CowDetailsViewModel cow;

    public CowDetailsFragment(CowDetailsViewModel cow) {
        this.cow = cow;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        return inflater.inflate(R.layout.cow_details_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ViewPager2 viewPager = view.findViewById(R.id.pager);
        viewPager.setAdapter(new CowDetailsAdapter(this, cow));

        TabLayout tabLayout = view.findViewById(R.id.tab_layout);

        new TabLayoutMediator(tabLayout, viewPager, (tab, position) -> tab.setText(tabs.get(position))).attach();
    }
}