package com.example.farmassistant.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.farmassistant.AppResources;
import com.example.farmassistant.OnItemClickedListener;
import com.example.farmassistant.R;
import com.example.farmassistant.activities.CowDetailsActivity;
import com.example.farmassistant.adapters.CowsAdapter;
import com.example.farmassistant.viewModels.CowViewModel;

import java.util.List;

public class CowsListFragment extends Fragment implements OnItemClickedListener {
    private final List<CowViewModel> cows;
    private final boolean showImage;

    public CowsListFragment(List<CowViewModel> cows, boolean showImage) {
        this.cows = cows;
        this.showImage = showImage;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        return inflater.inflate(R.layout.recycler_view_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recyclerView = requireView().findViewById(R.id.recycler_view);

        recyclerView.setAdapter(new CowsAdapter(cows, showImage, this));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
    }

    @Override
    public void onItemClick(View view, String id) {
        Intent intent = new Intent(getActivity(), CowDetailsActivity.class);
        intent.putExtra(AppResources.COW_ID_EXTRA_NAME, id);
        startActivity(intent);
    }
}
