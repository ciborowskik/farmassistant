package com.example.farmassistant.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.farmassistant.AppResources;
import com.example.farmassistant.OnItemClickedListener;
import com.example.farmassistant.R;
import com.example.farmassistant.activities.CowDetailsActivity;
import com.example.farmassistant.adapters.ToDoAdapter;
import com.example.farmassistant.viewModels.ToDoActionViewModel;

import java.util.List;

public class ToDoListFragment extends Fragment implements OnItemClickedListener {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter<ToDoAdapter.ViewHolder> adapter;


    private List<ToDoActionViewModel> actions;

    public ToDoListFragment(List<ToDoActionViewModel> actions) {
        this.actions = actions;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        return inflater.inflate(R.layout.recycler_view_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = requireView().findViewById(R.id.recycler_view);
        adapter = new ToDoAdapter(actions, this);

        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
    }


    @Override
    public void onItemClick(View view, String cowId) {
        Intent intent = new Intent(getActivity(), CowDetailsActivity.class);
        intent.putExtra(AppResources.COW_ID_EXTRA_NAME, cowId);
        startActivity(intent);
    }
}

