package com.example.farmassistant;

import android.view.View;

public interface OnItemClickedListener {
    void onItemClick(final View view, final String id);
}

