package com.example.farmassistant.dataSource;


import com.example.farmassistant.AppResources;
import com.example.farmassistant.viewModels.CowDetailsViewModel;
import com.example.farmassistant.viewModels.CowViewModel;
import com.example.farmassistant.viewModels.EditCowViewModel;
import com.example.farmassistant.viewModels.EventViewModel;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.WriteBatch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class CowDataSource extends DataSource {

    public static void addCow(EditCowViewModel cow, Consumer<String> onFinished) {
        Map<String, Object> serialized = serialize(cow);

        GetFirestore().collection(AppResources.cowsCollectionName)
                .add(serialized)
                .addOnSuccessListener(doc -> onFinished.accept(doc.getId()))
                .addOnFailureListener(exception -> onFinished.accept(null));
    }

    public static void editCow(EditCowViewModel cow, Consumer<Boolean> onFinished) {
        Map<String, Object> serialized = serialize(cow);

        GetFirestore().collection(AppResources.cowsCollectionName)
                .document(cow.getId())
                .set(serialized)
                .addOnSuccessListener(aVoid -> onFinished.accept(true))
                .addOnFailureListener(exception -> onFinished.accept(false));
    }

    public static void deleteCowWithEvents(String id, Consumer<Boolean> onFinished) {
        EventDataSource.getCowEventsViewModels(id, events -> {
            if (events == null) {
                onFinished.accept(false);
            }
            else {
                WriteBatch writeBatch = GetFirestore().batch();

                for (EventViewModel event : events){
                    writeBatch.delete(GetFirestore().collection(AppResources.eventsCollectionName).document(event.getId()));
                }

                writeBatch.delete(GetFirestore().collection(AppResources.cowsCollectionName).document(id));

                writeBatch.commit()
                        .addOnSuccessListener(aVoid -> onFinished.accept(true))
                        .addOnFailureListener(exception -> onFinished.accept(false));
            }
        });
    }

    public static void getCowsViewModels(Consumer<List<CowViewModel>> onFinished) {
        GetFirestore().collection(AppResources.cowsCollectionName)
                .whereEqualTo("ownerId", GetCurrentUser().getUid()).get()
                .addOnCompleteListener( task -> {
                    if (task.isSuccessful()) {
                        List<CowViewModel> cows = new ArrayList<>();

                        for (QueryDocumentSnapshot document : task.getResult()) {
                            cows.add(new CowViewModel(document.getId(), document.getData()));
                        }

                        Collections.sort(cows);

                        onFinished.accept(cows);
                    } else {
                        onFinished.accept(null);
                    }
                });
    }

    public static void getCowDetailsViewModel(String id, Consumer<CowDetailsViewModel> onFinished) {
        GetFirestore().collection(AppResources.cowsCollectionName)
                .document(id).get()
                .addOnCompleteListener( task -> {
                    if (task.isSuccessful()) {
                        CowDetailsViewModel cow = new CowDetailsViewModel(id, task.getResult().getData());
                        addMotherToDetailsViewModel(cow, onFinished);
                    } else {
                        onFinished.accept(null);
                    }
                });
    }

    private static void addMotherToDetailsViewModel(CowDetailsViewModel cow, Consumer<CowDetailsViewModel> onFinished) {
        if (cow.getMotherId() == null) {
            addChildrenToDetailsViewModel(cow, onFinished);
        }
        else {
            String motherId = cow.getMotherId();

            GetFirestore().collection(AppResources.cowsCollectionName)
                    .document(motherId).get()
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            CowViewModel mother = task.getResult().getData() != null
                                    ? new CowViewModel(motherId, task.getResult().getData())
                                    : null;
                            cow.setMother(mother);
                            addChildrenToDetailsViewModel(cow, onFinished);
                        } else {
                            onFinished.accept(null);
                        }
                    });
        }
    }

    private static void addChildrenToDetailsViewModel(CowDetailsViewModel cow, Consumer<CowDetailsViewModel> onFinished) {
        GetFirestore().collection(AppResources.cowsCollectionName)
                .whereEqualTo("motherId", cow.getId()).get()
                .addOnCompleteListener( task -> {
                    if (task.isSuccessful()) {
                        List<CowViewModel> children = new ArrayList<>();

                        for (QueryDocumentSnapshot document : task.getResult()) {
                            children.add(new CowViewModel(document.getId(), document.getData()));
                        }

                        Collections.sort(children);

                        cow.setChildren(children);
                        addEventsToDetailsViewModel(cow, onFinished);
                    } else {
                        onFinished.accept(null);
                    }
                });
    }

    private static void addEventsToDetailsViewModel(CowDetailsViewModel cow, Consumer<CowDetailsViewModel> onFinished) {
        EventDataSource.getCowEventsViewModels(cow.getId(), events -> {
            if (events == null) {
                onFinished.accept(null);
            } else {
                cow.setEvents(events);
                onFinished.accept(cow);
            }
        });
    }

    private static Map<String, Object> serialize(EditCowViewModel editCowViewModel){
        return new HashMap<String, Object>() {{
            put(AppResources.cowsOwnerIdFieldName, GetCurrentUser().getUid());
            put(AppResources.cowsNameFieldName, editCowViewModel.getName());
            put(AppResources.cowsNumberFieldName, editCowViewModel.getNumber());
            put(AppResources.cowsBirthDateFieldName, editCowViewModel.getBirthDate());
            put(AppResources.cowsMotherIdFieldName, editCowViewModel.getMother() != null ? editCowViewModel.getMother().getId() : null);
            put(AppResources.cowsCommentFieldName, editCowViewModel.getComment());
        }};
    }
}
