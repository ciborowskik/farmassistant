package com.example.farmassistant.dataSource;


import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

public class DataSource {

    protected static FirebaseFirestore GetFirestore() {
        return FirebaseFirestore.getInstance();
    }

    protected static FirebaseUser GetCurrentUser(){
        return FirebaseAuth.getInstance().getCurrentUser();
    }
}
