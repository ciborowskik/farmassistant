package com.example.farmassistant.dataSource;

import com.example.farmassistant.AppResources;
import com.example.farmassistant.viewModels.CowViewModel;
import com.example.farmassistant.viewModels.EditEventViewModel;
import com.example.farmassistant.viewModels.EventViewModel;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class EventDataSource extends DataSource {

    public static void addEvent(EditEventViewModel event, Consumer<String> onFinished) {
        Map<String, Object> serialized = serialize(event);

        GetFirestore().collection(AppResources.eventsCollectionName)
                .add(serialized)
                .addOnSuccessListener(doc -> onFinished.accept(doc.getId()))
                .addOnFailureListener(exception -> onFinished.accept(null));
    }

    public static void editEvent(EditEventViewModel event, Consumer<Boolean> onFinished) {
        Map<String, Object> serialized = serialize(event);

        GetFirestore().collection(AppResources.eventsCollectionName)
                .document(event.getId())
                .set(serialized)
                .addOnSuccessListener(doc -> onFinished.accept(true))
                .addOnFailureListener(exception -> onFinished.accept(false));
    }

    public static void deleteEvent(String id, Consumer<Boolean> onFinished) {
        GetFirestore().collection(AppResources.eventsCollectionName)
                .document(id)
                .delete()
                .addOnSuccessListener(aVoid -> onFinished.accept(true))
                .addOnFailureListener(exception -> onFinished.accept(false));
    }

    public static void getEventsViewModels(Consumer<List<EventViewModel>> onFinished) {
        GetFirestore().collection(AppResources.eventsCollectionName)
                .whereEqualTo("ownerId", GetCurrentUser().getUid()).get()
                .addOnCompleteListener( task -> {
                    if (task.isSuccessful()) {
                        List<EventViewModel> events = new ArrayList<>();

                        for (QueryDocumentSnapshot document : task.getResult()) {
                            events.add(new EventViewModel(document.getId(), document.getData()));
                        }

                        Collections.sort(events);

                        addCowsToEvents(events, onFinished);
                    } else {
                        onFinished.accept(null);
                    }
                });
    }

    public static void getCowEventsViewModels(String cowId, Consumer<List<EventViewModel>> onFinished) {
        GetFirestore().collection(AppResources.eventsCollectionName)
                .whereEqualTo("cowId", cowId).get()
                .addOnCompleteListener( task -> {
                    if (task.isSuccessful()) {
                        List<EventViewModel> events = new ArrayList<>();

                        for (QueryDocumentSnapshot document : task.getResult()) {
                            events.add(new EventViewModel(document.getId(), document.getData()));
                        }

                        Collections.sort(events);

                        onFinished.accept(events);
                    } else {
                        onFinished.accept(null);
                    }
                });
    }

    private static void addCowsToEvents(List<EventViewModel> events, Consumer<List<EventViewModel>> onFinished) {
        CowDataSource.getCowsViewModels(cows -> {
            if (cows == null) {
                onFinished.accept(null);
            } else {
                for (EventViewModel event : events){
                    CowViewModel cow = cows.stream().filter(c -> c.getId().equals(event.getCowId())).findFirst().get();
                    event.setCow(cow);
                }
                onFinished.accept(events);
            }
        });
    }

    public static void getEventViewModel(String id, Consumer<EventViewModel> onFinished) {
        GetFirestore().collection(AppResources.eventsCollectionName)
                .document(id).get()
                .addOnCompleteListener( task -> {
                    if (task.isSuccessful()) {
                        EventViewModel event = new EventViewModel(id, task.getResult().getData());
                        addCowInfo(event, onFinished);
                    } else {
                        onFinished.accept(null);
                    }
                });
    }

    private static void addCowInfo(EventViewModel event, Consumer<EventViewModel> onFinished) {
        GetFirestore().collection(AppResources.cowsCollectionName)
                .document(event.getCowId()).get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        event.setCow(new CowViewModel(event.getCowId(), task.getResult().getData()));
                        onFinished.accept(event);
                    } else {
                        onFinished.accept(null);
                    }
                });
    }

    private static Map<String, Object> serialize(EditEventViewModel editEventViewModel){
        return new HashMap<String, Object>() {{
            put(AppResources.eventsOwnerIdFieldName, GetCurrentUser().getUid());
            put(AppResources.eventsDateFieldName, editEventViewModel.getDate());
            put(AppResources.eventsCowIdFieldName, editEventViewModel.getCow() != null ? editEventViewModel.getCow().getId() : null);
            put(AppResources.eventsTypeFieldName, editEventViewModel.getType());
            put(AppResources.eventsCommentFieldName, editEventViewModel.getComment());
        }};
    }
}
