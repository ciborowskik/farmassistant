package com.example.farmassistant.viewModels;

public class ToDoActionViewModel {
    private final CowViewModel cow;
    private final String message;

    public ToDoActionViewModel(CowViewModel cow, String message) {
        this.cow = cow;
        this.message = message;
    }

    public CowViewModel getCow() {
        return cow;
    }

    public String getMessage() {
        return message;
    }
}
