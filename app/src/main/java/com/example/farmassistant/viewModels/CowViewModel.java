package com.example.farmassistant.viewModels;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.example.farmassistant.AppResources;
import com.google.firebase.Timestamp;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

public class CowViewModel implements Comparable<CowViewModel> {

    private final String id;
    private final String name;
    private final String number;
    private final String motherId;
    private final Date birthDate;

    public CowViewModel(String id, Map<String, Object> data) {
        this.id = id;
        this.name = (String)data.get(AppResources.cowsNameFieldName);
        this.number = (String)data.get(AppResources.cowsNumberFieldName);
        this.motherId = (String)data.get(AppResources.cowsMotherIdFieldName);
        this.birthDate = ((Timestamp)data.get(AppResources.cowsBirthDateFieldName)).toDate();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public String getMotherId() {
        return motherId;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public String getFormattedBirthDate() {
        if (this.birthDate == null){
            return "";
        }

        Format formatter = new SimpleDateFormat(AppResources.DATE_FORMAT, new Locale("pl", "PL"));
        return formatter.format(this.birthDate);
    }

    @NonNull
    @Override
    public String toString() {
        return !TextUtils.isEmpty(getNumber())
                ? String.format("(%s) %s", this.getNumber().substring(7, 12), this.getName())
                : getName();
    }

    @Override
    public int compareTo(CowViewModel other) {
        return other.getBirthDate().compareTo(birthDate);
    }
}
