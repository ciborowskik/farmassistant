package com.example.farmassistant.viewModels;

import androidx.annotation.NonNull;

import com.example.farmassistant.FarmAssistantApplication;
import com.example.farmassistant.R;


public enum EventType {
    BIRTH(R.string.event_type_birth),
    SEASON(R.string.event_type_season),
    IMPREGNATION(R.string.event_type_impregnation),
    DRYING(R.string.event_type_drying),
    VET(R.string.event_type_vet);

    private final int stringId;

    EventType(int stringId) {
        this.stringId = stringId;
    }

    @NonNull
    @Override
    public String toString() {
        return FarmAssistantApplication.getAppContext().getString(this.stringId);
    }
}
