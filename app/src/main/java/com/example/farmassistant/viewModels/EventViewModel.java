package com.example.farmassistant.viewModels;


import com.example.farmassistant.AppResources;
import com.google.firebase.Timestamp;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

public class EventViewModel implements Comparable<EventViewModel> {
    private String id;
    private Date date;
    private String cowId;
    private CowViewModel cow;
    private EventType type;
    private String comment;

    public EventViewModel(String id, Map<String, Object> data) {
        this.id = id;
        this.date = ((Timestamp)data.get(AppResources.eventsDateFieldName)).toDate();
        this.cowId = (String)data.get(AppResources.eventsCowIdFieldName);
        this.type = EventType.valueOf((String)data.get(AppResources.eventsTypeFieldName));
        this.comment = (String)data.get(AppResources.eventsCommentFieldName);
    }

    public void setCow(CowViewModel cow) {
        this.cow = cow;
    }

    public CowViewModel getCow() {
        return cow;
    }

    public String getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public String getCowId() {
        return cowId;
    }

    public EventType getType() {
        return type;
    }

    public String getComment() {
        return comment;
    }

    public String getFormattedDate() {
        if (this.date == null){
            return "";
        }

        Format formatter = new SimpleDateFormat(AppResources.DATE_FORMAT, new Locale("pl", "PL"));
        return formatter.format(this.date);
    }

    @Override
    public int compareTo(EventViewModel other) {
        return other.getDate().compareTo(date);
    }
}
