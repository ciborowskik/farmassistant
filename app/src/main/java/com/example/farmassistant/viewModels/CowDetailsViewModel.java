package com.example.farmassistant.viewModels;

import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.example.farmassistant.AppResources;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CowDetailsViewModel extends CowViewModel {
    private final String comment;
    private CowViewModel mother;
    private List<CowViewModel> children;
    private List<EventViewModel> events;

    public CowDetailsViewModel(String id, Map<String, Object> data) {
        super(id, data);
        this.comment = (String)data.get(AppResources.cowsCommentFieldName);
        this.mother = null;
        this.children = new ArrayList<>();
    }

    public String getComment() {
        return comment;
    }

    public CowViewModel getMother() {
        return mother;
    }

    public void setMother(CowViewModel mother) {
        this.mother = mother;
    }

    public List<CowViewModel> getChildren() {
        return children;
    }

    public void setChildren(List<CowViewModel> children) {
        this.children = children;
    }

    public List<EventViewModel> getEvents() {
        return events;
    }

    public void setEvents(List<EventViewModel> events) {
        this.events = events;
    }

    @NonNull
    @Override
    public String toString() {
        return !TextUtils.isEmpty(getNumber())
                ? String.format("(%s) %s", this.getNumber().substring(7, 12), this.getName())
                : getName();
    }
}
