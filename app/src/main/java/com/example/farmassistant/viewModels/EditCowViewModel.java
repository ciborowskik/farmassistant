package com.example.farmassistant.viewModels;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.example.farmassistant.AppResources;
import com.example.farmassistant.BR;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class EditCowViewModel extends BaseObservable {

    private List<CowViewModel> cows;
    private String id;
    private String name;
    private String number;
    private Date birthDate;
    private CowViewModel mother;
    private String comment;

    public EditCowViewModel() {
        this.cows = new ArrayList<>();
        this.id = null;
        this.birthDate = null;
        this.name = "";
        this.number = "";
        this.mother = null;
        this.comment = "";
    }

    public EditCowViewModel(CowDetailsViewModel detailsViewModel) {
        this.cows = new ArrayList<>();
        this.id = detailsViewModel.getId();;
        this.name = detailsViewModel.getName();
        this.number = detailsViewModel.getNumber();
        this.birthDate = detailsViewModel.getBirthDate();
        this.comment = detailsViewModel.getComment();
        this.mother = detailsViewModel.getMother();
    }

    public List<CowViewModel> getCows() {
        return cows;
    }

    public void setCows(List<CowViewModel> cows) {
        this.cows = cows;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
        notifyPropertyChanged(BR.formattedBirthDate);
    }

    @Bindable
    public CowViewModel getMother() {
        return mother;
    }

    public void setMother(CowViewModel mother) {
        this.mother = mother;
        notifyPropertyChanged(BR.mother);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Bindable
    public String getFormattedBirthDate() {
        if (this.birthDate == null){
            return "";
        }

        Format formatter = new SimpleDateFormat(AppResources.DATE_FORMAT, new Locale("pl", "PL"));
        return formatter.format(this.birthDate);
    }
}
