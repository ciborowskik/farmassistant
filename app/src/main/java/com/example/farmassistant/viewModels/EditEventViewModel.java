package com.example.farmassistant.viewModels;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.example.farmassistant.AppResources;
import com.example.farmassistant.BR;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class EditEventViewModel extends BaseObservable {
    private List<CowViewModel> cows;
    private List<EventType> eventTypes;
    private String id;
    private Date date;
    private CowViewModel cow;
    private EventType type;
    private String comment;

    public EditEventViewModel() {
        this.cows = new ArrayList<>();
        this.eventTypes = new ArrayList<>();
        this.id = null;
        this.date = null;
        this.cow = null;
        this.type = null;
        this.comment = "";
    }

    public EditEventViewModel(EventViewModel event) {
        this.cows = new ArrayList<>();
        this.eventTypes = new ArrayList<>();
        this.id = event.getId();
        this.date = event.getDate();
        this.cow = event.getCow();
        this.type = event.getType();
        this.comment = event.getComment();
    }

    public List<CowViewModel> getCows() {
        return cows;
    }

    public void setCows(List<CowViewModel> cows) {
        this.cows = cows;
    }

    public List<EventType> getEventTypes() {
        return eventTypes;
    }

    public void setEventTypes(List<EventType> eventTypes) {
        this.eventTypes = eventTypes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
        notifyPropertyChanged(BR.formattedDate);
    }

    @Bindable
    public CowViewModel getCow() {
        return cow;
    }

    public void setCow(CowViewModel cow) {
        this.cow = cow;
        notifyPropertyChanged(BR.cow);
    }

    @Bindable
    public EventType getType() {
        return type;
    }

    public void setType(EventType type) {
        this.type = type;
        notifyPropertyChanged(BR.type);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Bindable
    public String getFormattedDate() {
        if (this.date == null){
            return "";
        }

        Format formatter = new SimpleDateFormat(AppResources.DATE_FORMAT, new Locale("pl", "PL"));
        return formatter.format(this.date);
    }
}
